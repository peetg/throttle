module.exports = function(delay, fn) {
  var timeoutId;
  var last = 0;
  function now() {
    return +new Date();
  }

  return function() {
    var self = this;
    var elapsed = now() - last;
    var args = arguments;

    timeoutId && clearTimeout(timeoutId);

    function exec() {
      last = now();
      fn.apply(self, args);
    }

    if (elapsed > delay) {
      exec();
    } else {
      timeoutId = setTimeout(exec, delay - elapsed);
    }
  };
};
