# Usage

```js
var throttle = require('@c.p/throttle');

var throttled = throttle(1000, yourFunction);

// calling throttled multiple times will call yourFunction, at most, once per second
```
